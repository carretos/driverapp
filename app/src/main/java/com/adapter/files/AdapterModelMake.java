package com.adapter.files;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterModelMake extends RecyclerView.Adapter<AdapterModelMake.ViewHolder> {
    private ArrayList<HashMap<String, String>> modeList = new ArrayList<>();
    private OnItemClickListener listener;

    public void setModelList(ArrayList<HashMap<String, String>> makeList) {
        this.modeList = makeList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                android.R.layout.simple_list_item_activated_1, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterModelMake.ViewHolder holder, int position) {
        String vMake = modeList.get(position).get("vTitle");
        holder.vehicleName.setText(vMake);
    }

    @Override
    public int getItemCount() {
        return modeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView vehicleName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            vehicleName = itemView.findViewById(android.R.id.text1);
            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION && listener != null) {
                    HashMap<String, String> model = modeList.get(position);
                    listener.onItemClick(model);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(HashMap<String, String> vMake);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
