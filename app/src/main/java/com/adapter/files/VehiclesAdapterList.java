package com.adapter.files;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

public class VehiclesAdapterList extends RecyclerView.Adapter<VehiclesAdapterList.ViewHolder> {
    private ArrayList<HashMap<String, String>> makeList = new ArrayList<>();
    private OnItemClickListener listener;

    public void setMakeList(ArrayList<HashMap<String, String>> makeList) {
        this.makeList = makeList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                android.R.layout.simple_list_item_activated_1, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VehiclesAdapterList.ViewHolder holder, int position) {
        String vMake = makeList.get(position).get("vMake");
        holder.vehicleName.setText(vMake);
    }

    @Override
    public int getItemCount() {
        return makeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView vehicleName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            vehicleName = itemView.findViewById(android.R.id.text1);
            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION && listener != null) {
                    HashMap<String, String> vMake = makeList.get(position);
                    listener.onItemClick(vMake);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(HashMap<String, String> vMake);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
